#!/bin/sh

set -e

PKGNAME=freebsd103
WEB_SUBDIR=freebsd
PKGFILES=.

TSTAMP=`date "+%Y%m%d%H%M%S"`

tar --exclude './.git' -cyf /tmp/${PKGNAME}-sippy-${TSTAMP}.tbz2 ${PKGFILES}
for i in 1 2
do
  scp /tmp/${PKGNAME}-sippy-${TSTAMP}.tbz2 sobomax@download.sippysoft.com:/usr/local/www/data/${WEB_SUBDIR}/ || continue
  i=0
  break
done
if [ ${i} -ne 0 ]
then
  echo 'scp failed' >&2
  exit 1
fi
git tag rel.${TSTAMP}
git push origin rel.${TSTAMP}
