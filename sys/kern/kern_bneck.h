/*
 * Copyright (c) 2014 Sippy Software, Inc., http://www.sippysoft.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

struct recfilter_int {
    long a;
    long b;
    long scale;
    long scale_in;
    long lastval;
};

#define	CLASSI_DETECTOR		0
#define	CLASSII_DETECTOR	1

/* #define	KERN_SENDIT_BNECK */
/* #define	SENDIT_BNECK */

struct trace_point {
    u_long tp_clk;
    struct recfilter_int filter;
    const char *name;
    int detector_type;
};

#define RF_STATIC_INIT(_i_fcoef, _i_scale, _i_scale_out, _i_lastval) \
  {.lastval = (_i_lastval), .a = (_i_scale) - (_i_fcoef), .b = (_i_fcoef), \
   .scale = (_i_scale), .scale_in = (_i_scale * _i_scale_out)}

#define TP_STATIC_INIT(_i_dtype, _i_name) {.tp_clk = 1, .filter = \
  RF_STATIC_INIT(9991, 10000, 1000, 0), .name = (_i_name), \
  .detector_type = (_i_dtype)}

struct kern_bneck {
    u_long ref_clk;
    struct trace_point tps[15];
};

#define BN_STATIC_INIT() {.ref_clk = 0, .tps = {\
  TP_STATIC_INIT(CLASSI_DETECTOR, "test00"), TP_STATIC_INIT(CLASSI_DETECTOR, "test01"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test02"), TP_STATIC_INIT(CLASSI_DETECTOR, "test03"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test04"), TP_STATIC_INIT(CLASSI_DETECTOR, "test05"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test06"), TP_STATIC_INIT(CLASSI_DETECTOR, "test07"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test08"), TP_STATIC_INIT(CLASSI_DETECTOR, "test09"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test10"), TP_STATIC_INIT(CLASSI_DETECTOR, "test11"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test12"), TP_STATIC_INIT(CLASSI_DETECTOR, "test13"), \
  TP_STATIC_INIT(CLASSI_DETECTOR, "test14")}}

void kern_bneck_init(struct kern_bneck *, int);

u_long clk_tick(struct kern_bneck *);
long tp_tick(struct kern_bneck *, int, int);
long tp_getlastclk(struct trace_point *);
void tp_dump(struct kern_bneck *, long, long *, const char *);
void tp_dumpi(struct kern_bneck *, long, int *, const char *);

void recfilter_init(struct recfilter_int *, long, long, long, long);
long recfilter_apply(struct recfilter_int *, long);
